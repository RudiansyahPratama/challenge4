import React from 'react';

const UserName = props => (
    <p>{props.name}</p>
);

UserName.defaultProps = {
    name: "User"
}

export default UserName;